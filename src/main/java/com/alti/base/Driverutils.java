package com.alti.base;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.log4testng.Logger;

import com.alti.common.ConfigReader;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Driverutils {
	static Logger log = Logger.getLogger(Driverutils.class);
	public static WebDriver driver;
	DesiredCapabilities dcap;
	//public static AppiumDriver<WebElement> mobDriver;
	
	
	public static WebDriver getDriver() {
		return driver;
	}
	
	public static void launchApplication(String url,String browserType) throws Exception{
		initWebDriver(url,browserType);

	}
	
	
	public static void tearDown() {
		driver.quit();
	}
	
	
	
	public static String getURL() {
		if(ConfigReader.getProperty("weburl")==null){
			log.info("URL is undefined Stopping Test Execution");
			driver.quit();
				
		}
		return ConfigReader.getProperty("weburl");
	}
	
	private static void initWebDriver(String url,String browser) throws Exception {
		switch(browser) {
		case "chrome" :
			WebDriverManager.chromedriver().version("77.0.3865.40").setup();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("start-maximized"); 
			options.addArguments("enable-automation"); 
			options.addArguments("--no-sandbox"); 
			options.addArguments("--disable-infobars");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--disable-browser-side-navigation"); 
			options.addArguments("--disable-gpu"); 
			driver = new ChromeDriver(options); 
			driver.manage().timeouts().implicitlyWait(Long.parseLong(ConfigReader.getProperty("implicitWaitTime")), TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.navigate().to(getURL());
			break;
		case "firefox" :
			WebDriverManager.firefoxdriver().setup();
			System.setProperty("webdriver.gecko.driver", "/Users/skancherala⁩/Downloads/geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(Long.parseLong(ConfigReader.getProperty("implicitWaitTime")), TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.navigate().to(getURL());
			break;
		case "mobapp" :
			DesiredCapabilities cap= new DesiredCapabilities();
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, ConfigReader.getProperty("platform_name"));
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, ConfigReader.getProperty("device_name"));
			cap.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir")+"/mobapp/ticketmaster-app-na-debugProd.apk");
			driver= new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"),cap);
			break;
			
		}
	}

}
