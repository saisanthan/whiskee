package com.alti.constants;

import com.alti.common.ConfigReader;

public class Paths {
	static String exEnv= ConfigReader.getProperty("ExecutionEnv");
	public static final String BASE_URI="https://beta.tmol.co/api" ;
	public static final String NEXT  = "/next";

}
